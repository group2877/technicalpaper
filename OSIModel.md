# **OSI Model**

# Introduction

The Open Systems Interconnection Model or **OSI Model** describes how one computer communicates with another over the computer network. This model was developed in 1984 by the International Organization for Standardization ( **ISO** ). It was the first standard model for network communications adopted by all major computer and telecommunication companies. It is a 7-layer model with each layer having specific functionalities.

# The Seven Layers 
![OSI-Model](https://cdn.educba.com/academy/wp-content/uploads/2019/07/OSI-Model.png.webp)

* Each Layer is a package of protocols.

## 1. Application Layer
Network applications such as web browsers and email clients use this topmost layer. It provides protocols that enable these applications to send and receive information that is understandable to the end users.

Some of the protocols in the application layer include:
* **HTTP/HTTPS**(HyperText Markup Language/HTTP Secured)
* **FTP**(File Transfer Protocol)
* **SMTP**(Simple Mail Transfer Protocol)
* **DNS**(Domain Name System)
* **TELNET**(Teletype Network)
* etc. 

These protocols collectively form the Application Layer. They form the basis for network services like web surfing (using HTTP/HTTPS), emails(using SMTP), file transfer(FTP), virtual terminal(TELNET), etc.

## 2. Presentation Layer
This layer performs three significant functions: **translation**, **data compression** and **encryption**/**decryption**.

It first receives data from the application layer and converts the user-end data into machine-understandable binary language. Then the data is compressed for faster transmission. This compression can be either lossy or lossless. Then to secure the data, it is encrypted. On the sender's side, the data is encrypted and on the receiver's side, the data is decrypted. Finally, it is transmitted onto the next layer.

The **SSL**(Secure Sockets Layer) Protocol is used in this layer for encryption/decryption.

## 3. Session Layer
This layer helps in setting up connections that help in sending and receiving data. It creates communication channels called sessions between devices and ensures they are open as long as data is transferred between the devices and close after data is transmitted. It has command sets called Application Program Interfaces or APIs that help in providing session layer tools to higher layer protocols. NETBIOS(Network Basic Input/Output System), TCP/IP(Transmission Control Protocol/Internet Protocol) Sockets, and RPC(Remote Procedure Calls) are some examples of APIs.

The session layer mainly performs three functions: **Authentication**, **Authorization**, and **Session Management**.

* Authentication is essentially verifying who the user is. It is done by the usage of a username and password. Once the user is verified, a connection is established with the server.

* After authenticating the user, authorization is checked. It is the process used by the server to determine whether the user has permission to access a file.

* The session layer keeps a track of the data packets transmitted when downloading a file or using a browser. A web page contains text, images, etc. which are stored as separate files on the web server. When a website is requested in the web server, it opens a separate session to the webserver to download each of these files. These files are received in the form of data packets. The session layer keeps track of these data packets i.e. it helps in session management.

The web browser performs all functions of the session layer, presentation layer, and application layer.

## 4. Transport Layer
The transport layer controls the reliability of communications through **segmentation**, **flow control**, and **error control**. 

* The data received from the session layer is divided into small data units called **segments**. Each segment contains a source and destination port number and a sequence number. Port number helps in directing each segment into the correct application while sequence number helps in reassembling these segments to form the correct message to the receiving user.

* The transport layer controls the amount of data transmitted to a level that the receiver can process. Whenever the flow of data is greater than the flow the receiver can handle, the receiver with the help of the transport layer makes the flow optimal for the receiver. Similar is the case when the flow is lower than optimal.

* If some data units do not arrive at the destination, the transport layer uses Automatic Repair Request to retransmit the lost or corrupted data. For this purpose, a checksum(group of bits of 0s and 1s) is added to each data segment.

Protocols of Transmission Layer are **Transmission Control Protocol (TCP)** and **User Datagram Protocol(UDP)**
TCP is used for connection-oriented transmission while UDP is for connectionless transmission. UDP is faster than TCP but it does not give feedback on whether any data is missing or corrupt. Lost data can be re-transmitted in TCP as it provides feedback. 

* Examples for UDP transmission: Streaming movies, songs, online games, voice-over IP, TFTP, DNS, etc.
* Examples for TCP transmission: World wide web, Email, FTP, etc.

## 5. Network Layer
Transport Layer passes network segments through the Network Layer. Data units in the network layer are called **packets**. It is the layer where routers reside. Functions of the network layer are **Logical Addressing**, **Routing**, and **Path Determination**.

* IP addressing done in the network layer is called logical addressing. The network layer assigns the sender's and receiver's IP addresses in each segment to form an IP packet. IP addresses are assigned to ensure each data packet reaches the correct destination.

* Routing is the method to move the packet from source to destination and it is based on the logical addressing format of IPv4 and IPv6. Based on IP addresses and masks, routing decisions are taken in the network layer.

* Any computer is connected to the internet server in several ways. The network layer chooses the best possible path for data transmission using the protocols such as OSPF(Open Shortest Path First), BGP(Border Gateway Protocol), and IS-IS(Intermediate System to Intermediate System).

## 6. Data Link Layer
Data Link Layer receives data packets from the network layer. These packets are then assigned with sending and receiving MAC addresses or physical addresses by the data link layer to form a **frame**. MAC Address is a digit alphanumeric number embedded in the Network Interface Card (NIC) of a computer. The data link layer is embedded as software in the NIC. They provide means to transfer data via local media such as copper wires, optical fibers, or air.

Data Link Layer performs two basic functions. It allows upper layers of the OSI Model to access media using techniques such as framing. It controls how data is placed and received from the media using techniques such as Media Access Control and Error Detection.

## 7. Physical Layer
The frames from the data link layer are essentially bits made of 0s and 1s. The physical layer converts these bits into signals and transmits them over local media. The signals can be electrical signals(in the case of copper cables), light signals (in the case of optical fibers), or radio signals(in the case of air).

At the receiver's end, the physical layer receives signals and converts them to bits, and passes it to the data link layer as a frame and then to higher layers in the form of packets and segments and finally, it reaches the application layer. The application layer protocols make the sender's message visible in the application to the receiver.

# Advantages of OSI-Model
* It does not rely on a specific operating system.
* It encrypts data for secure data transmission.
* It helps network administrators to determine the required software and hardware to build their network.
* It helps us understand the complex functioning of a computer network by breaking it into simpler components.
* It helps network administrators troubleshoot issues more quickly and effectively by looking at the layer that is causing the issues.

# References
* https://www.youtube.com/watch?v=vv4y_uOneC0
* https://www.educba.com/what-is-osi-model/
* https://www.imperva.com/learn/application-security/osi-model/
* http://www.tcpipguide.com/free/t_SessionLayerLayer5.htm
* https://www.computernetworkingnotes.com/ccna-study-guide/osi-model-advantages-and-basic-purpose-explained.html
